package ru.edu.model;

import org.junit.Test;
import ru.edu.MapperUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class CatalogTest  {

    public static final String EXAMPLE_FILES_DIR = "./src/test/resources/example_files/";
    public static final String CATALOG_XML = "catalog.xml";
    public static final String CATALOG_JSON = "catalog.json";

    Catalog catalog = new Catalog();

    @Test
    public void testAddCD() throws IOException {

        catalog.addCD(
                new CD("Title 1",
                        "Name 1",
                        2020,
                        "USA")
        );
        catalog.addCD(
                new CD("Title 2",
                        "Name 2",
                        2021,
                        "Russia")
        );

        assertEquals(2, catalog.getCdList().size());

        MapperUtils.mapXml(EXAMPLE_FILES_DIR + CATALOG_XML, catalog);
        MapperUtils.mapJson(EXAMPLE_FILES_DIR + CATALOG_JSON, catalog);

    }

    @Test
    public void testReadCDJson() throws IOException {

        catalog = MapperUtils.readJson(EXAMPLE_FILES_DIR + CATALOG_JSON, Catalog.class);

        assertEquals(2, catalog.getCdList().size());
        assertEquals("Title 1", catalog.getCdList().get(0).getTitle());
        assertEquals("Name 1", catalog.getCdList().get(0).getName());
        assertEquals(2020, catalog.getCdList().get(0).getYear());
        assertEquals("USA", catalog.getCdList().get(0).getCountry());

        System.out.println(catalog.getCdList().get(0));

    }


    @Test
    public void testReadCDXml() throws IOException {

        catalog = MapperUtils.readXml(EXAMPLE_FILES_DIR + CATALOG_XML, Catalog.class);

        assertEquals(2, catalog.getCdList().size());
        assertEquals("Title 2", catalog.getCdList().get(1).getTitle());
        assertEquals("Name 2", catalog.getCdList().get(1).getName());
        assertEquals(2021, catalog.getCdList().get(1).getYear());
        assertEquals("Russia", catalog.getCdList().get(1).getCountry());

        System.out.println(catalog.getCdList().get(1));

    }

}