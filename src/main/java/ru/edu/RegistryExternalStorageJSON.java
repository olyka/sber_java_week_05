package ru.edu;

import ru.edu.model.Registry;

import java.io.IOException;

public class RegistryExternalStorageJSON implements RegistryExternalStorage {

    /**
     * Registry class variable.
     */
    private Registry registry = new Registry();

    /**
     * Empty constructor.
     */
    public RegistryExternalStorageJSON() {
    }

    /**
     * Constructor.
     * @param reg Registry object
     */
    public RegistryExternalStorageJSON(final Registry reg) {
        this.registry = reg;
    }

    /**
     * Чтение из файла.
     *
     * @param filePath путь до файла
     */
    @Override
    public void readFrom(final String filePath) throws IOException {
        registry = MapperUtils.readJson(filePath, Registry.class);
    }

    /**
     * Запись реестра в файл.
     *
     * @param filePath путь
     * @param reg реестр
     */
    @Override
    public void writeTo(final String filePath,
                        final Registry reg) throws IOException {
        MapperUtils.mapJson(filePath, reg);
    }

    /**
     * Запись реестра в файл.
     *
     * @param filePath путь
     */
    @Override
    public void writeTo(final String filePath) throws IOException {
        writeTo(filePath, registry);
    }

    /**
     * Get Registry object.
     * @return Registry object
     */
    public Registry getRegistry() {
        return registry;
    }

    /**
     * Set Registry object.
     * @param reg Registry object
     */
    public void setRegistry(final Registry reg) {
        this.registry = reg;
    }

}
