package ru.edu;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.File;
import java.io.IOException;

public final class MapperUtils {

    private MapperUtils() {
    }

    /**
     * Write object to file with XML format.
     * @param filename Filename
     * @param obj Object
     * @throws IOException IOException class object
     */
    public static void mapJson(final String filename,
                               final Object obj)
            throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(new File(filename), obj);

    }

    /**
     *
     * Read info from JSON file to object of known class.
     * @param filename Filename
     * @param clazz Class of object
     * @param <T> Object
     * @return Object of known class
     * @throws IOException IOException class object
     */
    public static <T> T readJson(final String filename,
                                 final Class<T> clazz)
            throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(new File(filename), clazz);

    }

    /**
     *
     * Write object to file with JSON format.
     * @param filename Filename
     * @param obj Object
     * @throws IOException IOException class object
     *
     */
    public static void mapXml(final String filename,
                              final Object obj)
            throws IOException {

        XmlMapper mapper = new XmlMapper();
        mapper.writeValue(new File(filename), obj);

    }

    /**
     *
     * Read info from XML file to object of known class.
     * @param filename Filename
     * @param clazz Class of object
     * @param <T> Object
     * @return Object of known class
     * @throws IOException IOException class object
     *
     */
    public static <T> T readXml(final String filename,
                                final Class<T> clazz)
            throws IOException {

        XmlMapper mapper = new XmlMapper();
        mapper.configure(
                DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
                false);

        return mapper.readValue(
                new File(filename),
                clazz);

    }

}
