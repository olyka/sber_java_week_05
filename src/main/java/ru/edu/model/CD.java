package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class CD {

    /**
     * CD's title (Name of the CD).
     */
    @JacksonXmlProperty(localName = "TITLE")
    private String title;

    /**
     * CD's artist.
     */
    @JacksonXmlProperty(localName = "ARTIST")
    private String name;

    /**
     * CD's year.
     */
    @JsonIgnore(value = false)
    @JacksonXmlProperty(localName = "YEAR")
    private int year;

    /**
     * CD's country.
     */
    @JacksonXmlProperty(localName = "COUNTRY")
    private String country;

    /**
     * Construct CD object.
     * @param cdTitle Title of album
     * @param cdName Name of the artist
     * @param cdYear CD's year
     * @param cdCountry CD's country
     */
    public CD(final String cdTitle,
              final String cdName,
              final int cdYear,
              final String cdCountry) {
        this.title = cdTitle;
        this.name = cdName;
        this.year = cdYear;
        this.country = cdCountry;
    }

    /**
     * Empty constructor.
     */
    public CD() {
    }

    /**
     * Get CD's title.
     * @return String with title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Get CD's artist name.
     * @return String with artist name
     */
    public String getName() {
        return name;
    }

    /**
     * Get CD's year.
     * @return int with year
     */
    public int getYear() {
        return year;
    }

    /**
     * Get CD's country.
     * @return String with country name
     */
    public String getCountry() {
        return country;
    }

    /**
     * Print CD object.
     * @return String with CD info
     */
    @Override
    public String toString() {
        return "CD{"
                + "title='" + title + '\''
                + ", name='" + name + '\''
                + ", year=" + year
                + ", country='" + country + '\''
                + '}';
    }
}
