package ru.edu.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.Serializable;

public class Album implements Serializable {

    /**
     * Album name.
     */
    @JacksonXmlProperty(localName = "name", isAttribute = true)
    private String name;

    /**
     * Album year.
     */
    @JacksonXmlProperty(localName = "year", isAttribute = true)
    private long year;

    /**
     * Constuct Album with name and year.
     * @param albumName
     * @param albumYear
     */
    public Album(final String albumName, final int albumYear) {
        this.name = albumName;
        this.year = albumYear;
    }

    /**
     * Empty constructor.
     */
    public Album() {
    }

    /**
     * Get Album name.
     * @return String with Album name
     */
    public String getName() {
        return name;
    }

    /**
     * Get Album year.
     * @return String with Album year
     */
    public long getYear() {
        return year;
    }

    /**
     * Print Album object to string.
     * @return String with Album info
     */
    @Override
    public String toString() {
        return "Album{"
                + "name='" + name + '\''
                + ", year=" + year
                + "}\n";
    }
}
