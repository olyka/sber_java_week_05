package ru.edu.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Country implements Serializable {

    /**
     * Country name.
     */
    @JacksonXmlProperty(localName = "name", isAttribute = true)
    private String name;

    /**
     * List of country artists.
     */
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "Artist")
    private final List<Artist> artists = new ArrayList<>();

    /**
     * Construct country object with name.
     * @param countryName Name of the country
     */
    public Country(final String countryName) {
        this.name = countryName;
    }

    /**
     * Empty constructor.
     */
    public Country() {
    }

    /**
     * Get country name.
     * @return String with country name
     */
    public String getName() {
        return name;
    }

    /**
     * Get list of country's artists.
     * @return List of Artist objects
     */
    public List<Artist> getArtists() {
        return artists;
    }

    /**
     * Add Artist object to list of country's artist.
     * @param artist Artist object
     */
    public void addArtist(final Artist artist) {
        artists.add(artist);
    }

    /**
     * Print Country info.
     * @return String with country info
     */
    @Override
    public String toString() {
        return "Country{"
                + "name='" + name + '\''
                + ", artists=" + artists
                + "}\n";
    }
}
