package ru.edu.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JacksonXmlRootElement(localName = "ArtistRegistry")
public class Registry implements Serializable {

    /**
     * Counter for countries.
     */
    @JacksonXmlProperty(localName = "countryCount", isAttribute = true)
    private int countryCount = 0;

    /**
     * List of countries.
     */
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "Country")
    private final List<Country> countries = new ArrayList<>();

    /**
     * Add country to list of countries.
     * Increase countries count.
     * @param country Country object
     */
    public void addCountry(final Country country) {
        countries.add(country);
        getCountryCount();
    }

    /**
     * Get list of countries.
     * @return List of Country objects
     */
    public List<Country> getCountries() {
        getCountryCount();
        return countries;
    }

    /**
     * Get number of countries in the list.
     * @return Number of countries
     */
    public int getCountryCount() {
        countryCount = countries.size();
        return countryCount;
    }

    /**
     * Print Registry info.
     * @return String with registry info
     */
    @Override
    public String toString() {
        return "Registry{"
                + "countryCount=" + countryCount
                + ", countries=" + countries
                + "}\n\n";
    }
}
