package ru.edu;

import ru.edu.model.Catalog;
import ru.edu.model.Registry;
import ru.edu.model.CD;
import ru.edu.model.Country;
import ru.edu.model.Artist;
import ru.edu.model.Album;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TransformCatalogToRegistry {

    /**
     * Catalog object variable.
     */
    private final Catalog catalog;

    /**
     * Registry object variable.
     */
    private final Registry registry;

    /**
     * Constructor.
     * @param cat Catalog object
     */
    public TransformCatalogToRegistry(final Catalog cat) {
        this.catalog = cat;
        this.registry = new Registry();
    }

    /**
     * Method to transform Catalog object to Registry object.
     * Grouping CD's by country and artist.
     * @return Registry object
     */
    public Registry transform() {

        Stream<CD> stream = catalog.getCdList().stream();

        Map<String, List<CD>> groupByCountryMap =
                stream.collect(Collectors.groupingBy(CD::getCountry));

        groupByCountryMap.forEach((countryName, countryList) -> {

            Country country = new Country(countryName);
            Map<String, List<CD>> groupByArtistMap
                    = countryList
                    .stream()
                    .collect(Collectors.groupingBy(CD::getName));
            groupByArtistMap.forEach((artistName, artistList) -> {
                Artist artist = new Artist(artistName);
                artistList.forEach(cd ->
                        artist.addAlbum(new Album(cd.getTitle(),
                                                cd.getYear())));
                country.addArtist(artist);
            });

            registry.addCountry(country);
        });

        return registry;
    }

}
